package config

/*	config.go berguna sebagai alat utk mengambil value konfigurasi dari config.json
*/

import (
	"github.com/tkanos/gonfig"
)

type Configuration struct {
	DB_USERNAME string
	DB_PASSWORD string
	DB_PORT	string
	DB_HOST string
	DB_NAME string
}

// Bikin sebuah function yg akan me-return instance dari struct Configuration 
func GetConfig() Configuration {
	// Variabel conf kita inisiasi dgn Configuration struct yg ada di atas
	conf := Configuration{}

	// Define file dari config.json, masukkan ke dalam variabel conf yg sudah didefinisikan sebelumnya
	gonfig.GetConf("config/config.json", &conf)
	return conf
}