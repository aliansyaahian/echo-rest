package routes

import (
	"echo-rest/controllers"
	"net/http"

	"github.com/labstack/echo"
	"echo-rest/middleware"
)

// Method Init yg akan me-return instance dgn tipe *echo.Echo
func Init() *echo.Echo {
	e := echo.New()

	e.GET("/", func(c echo.Context) error {
		// return c.JSON(http.StatusOK, "Hello, this is echo!")
		return c.String(http.StatusOK, "Hello, this is echo!")
	})

	// Belum pakai middleware
	// e.GET("/pegawai", controllers.FetchAllPegawai)
	// e.POST("/pegawai", controllers.StorePegawai)
	// e.PUT("/pegawai", controllers.UpdatePegawai)
	// e.DELETE("/pegawai", controllers.DeletePegawai)

	// Pakai middleware
	e.GET("/pegawai", controllers.FetchAllPegawai, middleware.IsAuthenticated)
	e.POST("/pegawai", controllers.StorePegawai, middleware.IsAuthenticated)
	e.PUT("/pegawai", controllers.UpdatePegawai, middleware.IsAuthenticated)
	e.DELETE("/pegawai", controllers.DeletePegawai, middleware.IsAuthenticated)

	// Generate manual password hash
	e.GET("/generate-hash/:password", controllers.GenerateHashPassword)
	e.POST("/login", controllers.CheckLogin)

	// Test validasi struct
	e.GET("/test-struct-validation", controllers.TestStructValidation)
	e.GET("/test-struct-variable", controllers.TestVariableValidation)

	return e
}
