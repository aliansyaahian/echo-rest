package controllers

import (
	"net/http"
	"echo-rest/models"
	"echo-rest/helpers"
	"github.com/labstack/echo"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"time"
)

func GenerateHashPassword(c echo.Context) error {
	password := c.Param("password")
	hash, _ := helpers.HashPassword(password)

	return c.JSON(http.StatusOK, hash)
}

func CheckLogin(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")

	res, err := models.CheckLogin(username, password)
	if err != nil {
		fmt.Println("Username/password salah")

		return c.JSON(http.StatusInternalServerError, map[string]string{
			"messages": err.Error(),
			// "messages": "Username/password salah",
		})
	}

	// Jika tdk ada error, kita cek yg di-return dari model CheckLogin apakah true atau false
	// Jika return false
	if !res {
		fmt.Println("Return false from model")

		// echo.ErrUnauthorized bawaan dari context
		return echo.ErrUnauthorized
		// return c.String(http.StatusInternalServerError, "Username/password salah")
	}

	
	// Jika tidak ada error, GENERATE TOKEN
	token := jwt.New(jwt.SigningMethodHS256)	// HS256 adalah algoritma hash JWT

	// Set claims
	claims := token.Claims.(jwt.MapClaims)

	// Di dalam MapClaims ini kita bisa setup payload-nya, informasi apa aja yg kita ingin simpan di dalam hash JWT
	claims["username"] =  username
	claims["level"] =  "application"
	claims["exp"] =  time.Now().Add(time.Hour * 72).Unix()	// expired

	// Generate encoded token and send it as response
	t, err := token.SignedString([]byte("secret"))		// utk prod bisa diganti jangan memakai string "secret"
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{
			"messages": err.Error(),
		})
	}

	return c.JSON(http.StatusOK, map[string]string{
		"token": t,
	})
}