package controllers

import (
	validator "github.com/go-playground/validator/v10"
	"github.com/labstack/echo"
	"net/http"
)

type Customer struct {
	Nama string `validate:"required"`			// NamaIsRequired
	Email string `validate:"required,email"`	// EmailIsRequired, EmailIsNotValid
	Alamat string `validate:"required"`			// AlamatIsRequired
	Umur int `validate:"gte=17,lte=35"`			// UmurGreaterThan17, UmurLessThan35
}

/*	Ada 2 jenis validasi:
	1. Validasi variabel 
	2. Validasi struct
*/

// Coba jenis validasi variabel
func TestVariableValidation(c echo.Context) error {
	v := validator.New()

	// Coba masukkan email yg tidak valid
	email := "ali"

	// Coba masukkan email yg valid
	// email := "aliansyaah@gmail.com"

	// v.Var(variable, rules-nya) utk memvalidasi single variable
	err := v.Var(email, "required,email")
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{
			// "message": err.Error(),
			"message": "Email not valid",
		})
	}

	return c.JSON(http.StatusOK, map[string]string{"message": "Success"})
}

// Coba jenis validasi struct
func TestStructValidation(c echo.Context) error {
	// Bikin object validator
	v := validator.New()

	// Coba masukkan data/nilai yg tidak valid ke dalam struct customer
	cust := Customer{
		Nama: "Ali",
		Email: "ali",
		Alamat: "",
		Umur: 15,
	}

	// Coba masukkan data/nilai yg valid ke dalam struct customer
	// cust := Customer{
	// 	Nama: "Ali",
	// 	Email: "aliansyaah@gmail.com",
	// 	Alamat: "Bandung",
	// 	Umur: 20,
	// }

	// Sambungkan antara validasinya dgn nilai struct customer
	err := v.Struct(cust)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{
			"message": err.Error(),
		})
	}

	return c.JSON(http.StatusOK, map[string]string{"message": "Success"})
}