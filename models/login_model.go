package models

import (
	"echo-rest/db"
	"echo-rest/helpers"
	"fmt"
	"database/sql"
)

type User struct {
	ID			int 	`json:"id"`
	Username	string 	`json:"username"`
}

func CheckLogin(username, password string) (bool, error) {
	var obj User
	var pwd string
	var defPwd string

	con := db.CreateCon()

	sqlStatement := "SELECT * FROM users WHERE username=?"
	err := con.QueryRow(sqlStatement, username).Scan(
		&obj.ID, &obj.Username, &pwd, &defPwd,
	)

	// Jika username salah / tdk ditemukan
	if err == sql.ErrNoRows {
		fmt.Println("Username not found")
		return false, err
	}

	if err != nil {
		fmt.Println("Query error")
		return false, err
	}

	// Check password hash
	// Param 1 plain password, Param 2 hash password
	match, err := helpers.CheckPasswordHash(password, pwd)
	
	// Jika password salah
	if !match {
		fmt.Println("Hash and password doesn't match.")
		return false, err
	}

	return true, nil
}