package models

import (
	"echo-rest/db"
	"net/http"
	validator "github.com/go-playground/validator/v10"
	"fmt"
)

type Pegawai struct {
	ID      int    `json:"id"`
	Nama    string `json:"nama" validate:"required"`
	Alamat  string `json:"alamat" validate:"required"`
	Telepon string `json:"telepon" validate:"required,gte=10,lte=12"`
}

func FetchAllPegawai() (Response, error) {
	// Bikin instansiasi object pegawai
	var obj Pegawai
	var arrObj []Pegawai
	var res Response

	// Bikin koneksinya
	con := db.CreateCon()

	sqlStatement := "SELECT * FROM pegawai"
	rows, err := con.Query(sqlStatement)
	defer rows.Close()

	// Jika ada error, return saja errornya krn akan dihandle di controller
	if err != nil {
		// panic("sqlStatement error")
		return res, err
	}

	// Jika tidak ada error, handle datanya
	for rows.Next() {
		// Kita coba scan, masukkan ke masing-masing kolom dalam object Pegawai
		err = rows.Scan(&obj.ID, &obj.Nama, &obj.Alamat, &obj.Telepon)
		if err != nil {
			return res, err
		}

		arrObj = append(arrObj, obj)
	}

	// Set up return
	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = arrObj

	return res, nil
}

// Coba bikin custom notifikasi error
func notifError(field string, tag string) string {
	var pesan string

	switch {
		case (field == "Nama") && (tag == "required"):
			pesan = "Kolom nama harus diisi"
		case (field == "Alamat") && (tag == "required"):
			pesan = "Kolom alamat harus diisi"
		case (field == "Telepon") && (tag == "gte"):
			pesan = "Nomor telepon terlalu pendek"
		case (field == "Telepon") && (tag == "lte"):
			pesan = "Nomor telepon terlalu panjang"
		default:
			pesan = "Pesan error tidak terdefinisi"
	}

	return pesan
}

func StorePegawai(nama string, alamat string, telepon string) (Response, error) {
	var res Response
	var pesan []string

	v := validator.New()	// inisiasi validator
	validatePegawai := Pegawai{
		Nama: nama,
		Alamat: alamat,
		Telepon: telepon,
	}

	err := v.Struct(validatePegawai)
	if err != nil {
		// Looping detail error
		for _, err := range err.(validator.ValidationErrors){
			fmt.Println("Namespace:", err.Namespace())
			fmt.Println("Field:", err.Field())
			fmt.Println("StructNamespace:", err.StructNamespace())
			fmt.Println("StructField:", err.StructField())
			fmt.Println("Tag:", err.Tag())
			fmt.Println("ActualTag:", err.ActualTag())
			fmt.Println("Kind:", err.Kind())
			fmt.Println("Type:", err.Type())
			fmt.Println("Value:", err.Value())
			fmt.Println("Param:", err.Param())
			fmt.Println()

			// Coba bikin custom notifikasi error
			pesan = append(pesan, notifError(err.Field(), err.Tag()))
		}
		
		fmt.Println(pesan)
		res.Data = pesan

		return res, err
	}

	con := db.CreateCon()

	sqlStatement := "INSERT pegawai (nama, alamat, telepon) VALUES (?, ?, ?)"
	// Mempersiapkan sqlStatement kita agar dapat dieksekusi
	stmt, err := con.Prepare(sqlStatement)
	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(nama, alamat, telepon)
	if err != nil {
		return res, err
	}

	// Ambil last inserted ID
	lastInsertedID, err := result.LastInsertId()
	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	
	// Data kita isi dgn map[string]int64 krn kita ingin mengembalikan lastInsertedID
	res.Data = map[string]int64{
		"last_inserted_id": lastInsertedID,
	}

	return res, nil
}

func UpdatePegawai(id int, nama string, alamat string, telepon string) (Response, error) {
	var res Response
	con := db.CreateCon()

	sqlStatement := "UPDATE pegawai SET nama=?, alamat=?, telepon=? WHERE id=?"
	stmt, err := con.Prepare(sqlStatement)
	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(nama, alamat, telepon, id)
	if err != nil {
		return res, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = map[string]int64{
		"rows_affected": rowsAffected,
	}

	return res, nil
}

func DeletePegawai(id int) (Response, error) {
	var res Response
	con := db.CreateCon()

	sqlStatement := "DELETE FROM pegawai WHERE id=?"
	stmt, err := con.Prepare(sqlStatement)
	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(id)
	if err != nil {
		return res, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = map[string]int64{
		"rows_affected": rowsAffected,
	}

	return res, nil
}