package db

import (
	"database/sql"
	"echo-rest/config"
	_ "github.com/go-sql-driver/mysql"
	// "fmt"
)

var db *sql.DB 
var err error

func Init() {
	// Variabel conf gunanya utk mengambil configuration file yg di-return oleh file config.go
	conf := config.GetConfig()

	// username:password@protocol(address:port)/dbname
	connectionString := conf.DB_USERNAME + ":" + conf.DB_PASSWORD + "@tcp(" + conf.DB_HOST + ":" + conf.DB_PORT + ")/" + conf.DB_NAME

	// sql.Open("drivername", dsn)
	db, err = sql.Open("mysql", connectionString)	// buka koneksi mysql
	if err != nil {
		panic("connectionString error!")
	}

	// db.Ping() untuk mengecek apakah connectionString sudah oke atau belum
	err = db.Ping()
	if err != nil {
		panic("DSN invalid!")
	}
}

// Bikin function utk me-return instance dari DB yg sudah dibuat di fungsi Init
func CreateCon() *sql.DB {
	return db
}