package helpers

import (
	"golang.org/x/crypto/bcrypt"
)

// Meng-hash plain password
func HashPassword(password string) (string, error) {
	// GenerateFromPassword(string password dlm bentuk byte, int cost(berupa angka utk proses hashing))
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(bytes), err
}

// Mengecek password
func CheckPasswordHash(password, hash string) (bool, error) {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		return false, err
	}

	return true, nil
}