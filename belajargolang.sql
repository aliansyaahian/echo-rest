/*
 Navicat Premium Data Transfer

 Source Server         : MySQL Local
 Source Server Type    : MySQL
 Source Server Version : 100119
 Source Host           : localhost:3306
 Source Schema         : belajargolang

 Target Server Type    : MySQL
 Target Server Version : 100119
 File Encoding         : 65001

 Date: 13/11/2020 16:48:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for mahasiswa
-- ----------------------------
DROP TABLE IF EXISTS `mahasiswa`;
CREATE TABLE `mahasiswa`  (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `nim` bigint(14) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `semester` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` datetime(0) NOT NULL,
  `updated_at` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mahasiswa
-- ----------------------------
INSERT INTO `mahasiswa` VALUES (1, 12345, 'ali', '2', '2020-09-01 15:51:24', '2020-09-02 11:41:42');
INSERT INTO `mahasiswa` VALUES (2, 98765, 'Bambang', '4', '2020-09-15 19:34:57', '2020-09-15 19:34:57');
INSERT INTO `mahasiswa` VALUES (3, 23456, 'Regina', '6', '2020-09-15 19:53:21', '2020-09-15 19:53:21');
INSERT INTO `mahasiswa` VALUES (4, 34567, 'Putri', '6', '2020-09-15 20:41:48', '2020-09-15 20:41:48');
INSERT INTO `mahasiswa` VALUES (6, 45679, 'Olegun', '1', '2020-09-16 11:27:22', '2020-09-16 11:27:22');

-- ----------------------------
-- Table structure for pegawai
-- ----------------------------
DROP TABLE IF EXISTS `pegawai`;
CREATE TABLE `pegawai`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `telepon` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pegawai
-- ----------------------------
INSERT INTO `pegawai` VALUES (1, 'Ali', 'Bandung', '081234567890');
INSERT INTO `pegawai` VALUES (2, 'Egin', 'Bandung', '081345678901');
INSERT INTO `pegawai` VALUES (3, 'Vale', 'Jakarta', '081123456789');
INSERT INTO `pegawai` VALUES (4, 'Nina', 'Semarang', '081234567890');
INSERT INTO `pegawai` VALUES (5, 'Jack', 'Jakarta', '021');
INSERT INTO `pegawai` VALUES (6, 'Nono', 'Tasik', '081234567891');
INSERT INTO `pegawai` VALUES (7, 'Wawan', 'Depok', '081345678901');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `default_pass` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'aliansyah', '$2a$10$.LhtNWJOjxIrwAqMmA0ioemocn0Z3X.4.INMqXG3QV8jJHTn8VsAu', '123');
INSERT INTO `users` VALUES (2, 'alibaba', '$2a$10$ch3aaXvenuTHELiIAv6lb.umuzWoKoqK5eaBlWo0tgKfk6atMKyDy', '1234');

SET FOREIGN_KEY_CHECKS = 1;
