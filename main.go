package main

import (
	// "net/http"
	// "github.com/labstack/echo"
	"echo-rest/db"
	"echo-rest/routes"
)

func main() {
	// Panggil method Init() pada package db
	db.Init()

	e := routes.Init()	// panggil method Init() pada package routes
	
	// e.Logger.Fatal(e.Start(":1234"))
	e.Logger.Fatal(e.Start("localhost:1234"))
}